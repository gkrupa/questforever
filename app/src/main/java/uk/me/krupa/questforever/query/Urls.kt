package uk.me.krupa.questforever.query

import android.net.Uri

object Urls {

    private const val BASE_URL = "https://census.daybreakgames.com/s:questforever/"
    private const val MAX_RESULTS = 100

    fun guildSearch(name: String): Uri =
            searchUrl("guild", MAX_RESULTS, mapOf(
                    "name" to "i/$name/",
                    "c:show" to "name,id,world")
            )

    fun toonSearch(name: String): Uri =
            searchUrl("character", MAX_RESULTS, mapOf(
                    "displayname" to "^$name",
                    "c:sort" to "displayname",
                    "c:show" to "id,name,guild.rank,type,tradeskills,locationdata"
            ))

    fun itemSearch(name: String): Uri =
            searchUrl("item", MAX_RESULTS, mapOf(
                    "displayname" to "i/$name/",
                    "c:sort" to "displayname",
                    "c:show" to "id,displayname,tier,type,iconid"
            ))

    fun toonPortrait (id: Long): Uri =
            Uri.parse(BASE_URL).buildUpon()
                    .appendPath("img")
                    .appendPath("eq2")
                    .appendPath("character")
                    .appendPath(id.toString(10))
                    .appendPath("headshot")
                    .build()

    fun itemIcon (id: Long): Uri =
            Uri.parse(BASE_URL).buildUpon()
                    .appendPath("img")
                    .appendPath("eq2")
                    .appendPath("icons")
                    .appendPath(id.toString(10))
                    .appendPath("item")
                    .build()

    fun searchUrl(kind: String, limit: Int, params: Map<String,String>): Uri =
        Uri.parse(BASE_URL).buildUpon()
                .appendPath("get")
                .appendPath("eq2")
                .appendPath(kind)
                .appendQueryParameter("c:limit", limit.toString(10))
                .apply {
                    for((key, value) in params) {
                        appendQueryParameter(key, value)
                    }
                }.build()

    fun getUrl(kind: String, id: String, params: Map<String,String>): Uri =
        Uri.parse(BASE_URL).buildUpon()
                .appendPath("get")
                .appendPath("eq2")
                .appendPath(kind)
                .appendPath(id)
                .apply {
                    for((key, value) in params) {
                        appendQueryParameter(key, value)
                    }
                }.build()

}