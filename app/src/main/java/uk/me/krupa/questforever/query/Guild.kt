package uk.me.krupa.questforever.query

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import uk.me.krupa.questforever.data.ItemType
import uk.me.krupa.questforever.data.SearchResult
import java.net.URL

object Guild {

    val client = OkHttpClient()

    fun search(name: String): List<SearchResult> {

        val url = URL(Urls.guildSearch(name).toString())
        Log.i("query.Guild", url.toString())

        val request = Request.Builder()
                .get()
                .url(url)
                .build()

        val json = JSONObject(client.newCall(request).execute().body()?.string())
        Log.i("query.Guild", json.toString(2))

        val items = json["guild_list"] as JSONArray
        return List(items.length()) {
            val guild = items[it] as JSONObject
            SearchResult(
                    ItemType.GUILD,
                    guild.getString("id"),
                    guild.getString("name"),
                    "Guild of " + guild.getString("world")
            )
        }
    }
}