package uk.me.krupa.questforever.query

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import uk.me.krupa.questforever.data.ItemType
import uk.me.krupa.questforever.data.SearchResult
import java.net.URL

object Toon {

    val client = OkHttpClient()

    fun search(name: String): List<SearchResult> {

        val url = URL(Urls.toonSearch(name).toString())
        Log.i("query.Toon", url.toString())

        val request = Request.Builder()
                .get()
                .url(url)
                .build()

        val json = JSONObject(client.newCall(request).execute().body()?.string())
        Log.i("query.Toon", json.toString(2))

        val items = json["character_list"] as JSONArray
        return List(items.length()) {
            val toon = items[it] as JSONObject
            SearchResult(
                    ItemType.TOON,
                    toon.getString("id"),
                    name(toon.getJSONObject("name")),
                    toon.getJSONObject("type").getString("class") + " of " +
                            toon.getJSONObject("locationdata").getString("world"),
                    Urls.toonPortrait(toon.getLong("id")).toString()
            )
        }
    }

    fun name(block: JSONObject): String =
        if (block.has("last")) {
            block.getString("first") + " " + block.getString("last")
        } else {
            block.getString("first")
        }

}