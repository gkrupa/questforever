package uk.me.krupa.questforever.query

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import uk.me.krupa.questforever.data.ItemType
import uk.me.krupa.questforever.data.SearchResult
import java.net.URL

object Item {

    val client = OkHttpClient()

    fun search(name: String): List<SearchResult> {

        val url = URL(Urls.itemSearch(name).toString())
        Log.i("query.Item", url.toString())

        val request = Request.Builder()
                .get()
                .url(url)
                .build()

        val json = JSONObject(client.newCall(request).execute().body()?.string())
        Log.i("query.Item", json.toString(2))

        val items = json["item_list"] as JSONArray
        return List(items.length()) {
            val item = items[it] as JSONObject
            val imageUrl = Urls.itemIcon(item.getLong("iconid")).toString()
            Log.i("query.Item", imageUrl)
            SearchResult(
                    ItemType.EQUIPMENT,
                    item.getString("id"),
                    item.getString("displayname"),
                    if (item.getString("tier") ?: "" == "") {
                        item.getString("type")
                    } else {
                        item.getString("tier") + " " + item.getString("type")
                    },
                    imageUrl
            )
        }
    }

}