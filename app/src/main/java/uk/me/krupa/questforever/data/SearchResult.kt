package uk.me.krupa.questforever.data

data class SearchResult(
        val type: ItemType,
        val id: String,
        val name: String,
        val additional: String,
        var iconUrl: String = "",
        var favourite: Boolean = false)