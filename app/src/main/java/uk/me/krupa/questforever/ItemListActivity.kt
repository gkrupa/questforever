package uk.me.krupa.questforever

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.Main
import okhttp3.OkHttpClient
import uk.me.krupa.questforever.data.SearchResult
import uk.me.krupa.questforever.query.Guild
import uk.me.krupa.questforever.query.Item
import uk.me.krupa.questforever.query.Toon
import uk.me.krupa.questforever.query.Urls
import uk.me.krupa.questforever.ui.SearchResultViewAdapter
import java.net.URL

/**
 * Activity representing a list of things (Guilds, Characters, Equipment).
 * @author Gerard Krupa
 */
class ItemListActivity : AppCompatActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private val client = OkHttpClient()

    private var results = mutableListOf<SearchResult>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        searchBar.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                GlobalScope.launch(Dispatchers.Main) {
                    searchBar.clearFocus()
                    searchBar.visibility = View.GONE
                    busy_frame.visibility = View.VISIBLE
                    list_frame.visibility = View.INVISIBLE

                    try {
                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(searchBar.windowToken, 0)
                        results.clear()
                        item_list.adapter?.notifyDataSetChanged()

                        val url = URL(Urls.guildSearch(query ?: "").toString())
                        Log.i("Query", url.toString())
                        async(Dispatchers.IO) {
                            query?.apply {
                                results.addAll(Guild.search(this))
                                results.addAll(Toon.search(this))
                                results.addAll(Item.search(this))
                            }
                        }.await()

                        item_list.adapter?.notifyDataSetChanged()
                    } finally {
                        searchBar.isEnabled = true
                        busy_frame.visibility = View.INVISIBLE
                        list_frame.visibility = View.VISIBLE
                        searchBar.visibility = View.VISIBLE
                    }
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (TextUtils.isEmpty(newText)) {
                    Log.i("Bert", "Cleared")
                    results.clear()
                    item_list.adapter?.notifyDataSetChanged()
                }
                return true
            }

        })

        if (item_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setupRecyclerView(item_list)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = SearchResultViewAdapter(this, results, twoPane)
    }


}
