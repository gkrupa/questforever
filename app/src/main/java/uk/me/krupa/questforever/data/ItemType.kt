package uk.me.krupa.questforever.data

enum class ItemType {
    GUILD,
    TOON,
    EQUIPMENT,
    SERVER
}