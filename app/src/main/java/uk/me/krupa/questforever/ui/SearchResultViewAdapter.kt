package uk.me.krupa.questforever.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_content.view.*
import uk.me.krupa.questforever.ItemDetailActivity
import uk.me.krupa.questforever.ItemDetailFragment
import uk.me.krupa.questforever.R
import uk.me.krupa.questforever.data.ItemType
import uk.me.krupa.questforever.data.SearchResult
import java.lang.Exception

const val LOG_TAG = "ui.SearchResult"

class SearchResultViewAdapter(private val parentActivity: AppCompatActivity,
                              private val values: List<SearchResult>,
                              private val twoPane: Boolean) :
        RecyclerView.Adapter<SearchResultViewAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as SearchResult
            if (twoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putString(ItemDetailFragment.ARG_ITEM_ID, item.id)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                    putExtra(ItemDetailFragment.ARG_ITEM_ID, item.id)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.name
        holder.contentView.text = item.additional

        if (item.favourite) {
            holder.favouriteToggle.setImageDrawable(ContextCompat.getDrawable(holder.favouriteToggle.context, android.R.drawable.btn_star_big_on))
        } else {
            holder.favouriteToggle.setImageDrawable(ContextCompat.getDrawable(holder.favouriteToggle.context, android.R.drawable.btn_star_big_off))
        }

        holder.favouriteToggle.setOnClickListener { view ->
            item.favourite = !item.favourite
            if (item.favourite) {
                holder.favouriteToggle.setImageDrawable(ContextCompat.getDrawable(holder.favouriteToggle.context, android.R.drawable.btn_star_big_on))
            } else {
                holder.favouriteToggle.setImageDrawable(ContextCompat.getDrawable(holder.favouriteToggle.context, android.R.drawable.btn_star_big_off))
            }
        }

        if (item.type == ItemType.GUILD) {
            holder.itemIcon.setImageResource(R.mipmap.ic_launcher_foreground)
        } else if (item.iconUrl != "") {
            Picasso
                    .get()
                    .load(item.iconUrl)
                    .resizeDimen(android.R.dimen.app_icon_size, android.R.dimen.app_icon_size)
                    .centerCrop()
                    .error(android.R.drawable.ic_menu_close_clear_cancel)
                    .placeholder(android.R.drawable.ic_menu_view)
                    .into(holder.itemIcon, object : Callback {
                        override fun onSuccess() {
                            if (Log.isLoggable(LOG_TAG, Log.DEBUG)) {
                                Log.d(LOG_TAG, "Downloaded ${item.iconUrl}")
                            }
                        }

                        override fun onError(e: Exception?) {
                            Log.w(LOG_TAG, "Failed to download ${item.iconUrl}", e)
                        }
                    })
        } else {
            holder.itemIcon.setImageResource(android.R.drawable.ic_menu_close_clear_cancel)
        }

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val favouriteToggle: ImageButton = view.favourite_button
        val idView: TextView = view.id_text
        val contentView: TextView = view.content
        val itemIcon: ImageView = view.item_icon
    }
}